import tensorflow as tf
import numpy as np
import os
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Conv2D, MaxPooling2D, Dropout
from keras.activations import relu, softmax
from sklearn.preprocessing import LabelBinarizer
from keras.losses import binary_crossentropy
from scipy import ndimage, misc
from PIL import Image
from skimage.transform import resize
from skimage import io

PROVA = 8 
(IMG_WIDTH, IMG_HEIGHT) = (64,64)

TRAINING_PATH = "./GTSRB_train/Final_Training/Images"
OTHER_IMAGES = "./immagini_nuove"

x_train_, y_train_ = [],[]

for road_sign in os.listdir(TRAINING_PATH):
    current_repository = TRAINING_PATH + "/" + road_sign
    if(not os.path.isdir(current_repository)):
        continue
    for file in os.listdir(current_repository):
        if file.endswith(".ppm"):
            image = plt.imread(current_repository+ "/" + file)
            image = resize(image, (IMG_WIDTH, IMG_HEIGHT))
            x_train_.append(image)
            y_train_.append(int(road_sign))
            

for current_image in os.listdir(OTHER_IMAGES):
    image = plt.imread(OTHER_IMAGES + "/" + current_image)
    image = resize(image, (IMG_WIDTH, IMG_HEIGHT))
    x_train_.append(image)
    y_train_.append(43)


x_train = np.array(x_train_)
y_train = np.array(y_train_)

shuffle_index = np.arange(x_train.shape[0])
np.random.shuffle(shuffle_index)

x_train = x_train[shuffle_index]
y_train = y_train[shuffle_index]

label_binarizer = LabelBinarizer()
y_one_hot = label_binarizer.fit_transform(y_train)

CLASSES = 44
EPOCHS = 10
BATCH_SIZE = 265

n_rows, n_cols, n_channels = (IMG_HEIGHT,IMG_WIDTH,3)

img_shape = (n_rows, n_cols, n_channels)
n_filters = 16
kernel_size = (3,3)

model = Sequential()
model.add(Conv2D(n_filters, kernel_size, strides=(1, 1), padding='valid', input_shape=img_shape,
        use_bias=True, kernel_initializer='random_uniform', bias_initializer='zeros'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2), padding='valid'))
model.add(Conv2D(n_filters*2, kernel_size, strides=(1, 1), padding='valid', use_bias=True,
        kernel_initializer='random_uniform', bias_initializer='zeros'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2), padding='valid'))
model.add(Dropout(0.3))
model.add(Flatten())
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dense(CLASSES))
model.add(Activation('softmax'))

model.compile(loss=binary_crossentropy,
              optimizer='adam',
              metrics=['accuracy'])

history = model.fit(x_train, y_one_hot,
                   batch_size = BATCH_SIZE,
                   epochs = EPOCHS,
                   validation_split=0.2,
                   verbose=2)

model.save("modello_prova"+str(PROVA)+".h5")
plt.plot(history.history['acc'], label='train')
plt.plot(history.history['val_acc'], label='validation')
plt.legend()
plt.savefig('plot_prova'+str(PROVA)+'.png')
plt.show()
