import cv2
from matplotlib import pyplot as plt
import numpy as np
from keras.models import load_model
from skimage.transform import resize

(IMG_WIDTH, IMG_HEIGHT) = (64,64)

model = load_model('/home/arianna/Scrivania/Università/Progetto Finale/germanTS/RECOGNITION/modello_prova8.h5')


cam = cv2.VideoCapture(0)
while True:
    image = cam.read()[1]
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray_image,(5,5),0)

    edges = cv2.Canny(blur,30,200)
    
    cv2.imshow("Edges",edges)

    image2, contours, hierarchy = cv2.findContours(edges, 
            cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    cv2.imshow("image2",image2)

    bounding_boxes = []

    for i in range(len(contours)):
        perimeter = cv2.arcLength(contours[i],True)
        approx = cv2.approxPolyDP(contours[i], 0.1 * perimeter, True)
        (x,y,w,h) = (0,0,0,0)
        print("len(approx["+str(i)+"]) = "+str(len(approx)))
        if(len(approx) >= 3):
            (x,y,w,h) = cv2.boundingRect(approx)
            bounding_boxes.append((x,y,w,h))
    
    for rectangle in bounding_boxes:
        sub_image = image[rectangle[0]:rectangle[0]+rectangle[2],
                rectangle[1]:rectangle[1]+rectangle[3]]
        try:
            input_image = resize(sub_image, (IMG_WIDTH, IMG_HEIGHT))
            input_image = np.expand_dims(input_image,axis=0)
        except ValueError:
            continue
        result = np.append([],model.predict(input_image))
        for j in range(43):
            if((result[j]) > 0.7):
                shape = j
                cv2.putText(image,str(shape),(rectangle[0],rectangle[1]),
                        cv2.FONT_HERSHEY_SIMPLEX, 4,(255,255,255),2,cv2.LINE_AA)
                cv2.rectangle(image,(rectangle[0],rectangle[1]),(rectangle[0]+rectangle[2],
                        rectangle[1]+rectangle[3]),(0,255,0))
    
    cv2.imshow("Camera",image)
    if(cv2.waitKey(5) == 32):
        break
